So let's talk about FIFO queues. And FIFO queues, we've seen them in the console, but now we're going to a deep dive in it.
They're increasingly popular at the exam, and I will show you what is important to understand. So this is a newer offering.
FIFO means first-in, first-out, and this is not available in all regions, but AWS is working hard to make it available in most regions.
The name of the queue must end in .fifo. Otherwise, you will not be able to create the queue as we'll see in the hands-on.
There will be a lower throughput. You can have up to 3,000 messages per second if you enable batching, or 300 messages per second without batching.
So you decrease your throughput, and you increase your guarantee at ordering with the FIFO queues. The messages will be processed in order by the consumer,
so that means that if you send a message, and then another one, the consumer will see them in the exact same order.
The messages will be sent exactly once. And you cannot introduce a per-message delay, but only a per-queue delay because if you work
to introduce a per-message delay, that would mean that the FIFO guarantee would be wrong. So what does it look like?
Here's our FIFO queue, and we send a message and another one, a third one, a fourth one, and a fifth one.
And, as you can see, they're all in order in the queue waiting to be processed one by one. So the order of the reads for the consumer part
is going to be the same, 1, 2, 3, 4, 5. This is something that a standard queue does not give you.
The standard queue, remember, does give you a best effort for the ordering but does not guarantee any kind of ordering.
So, now let's talk about the exam points that comes up for SQS FIFO. So, the first one is around deduplication,
or how not to send the same message twice. So, when you send a message to SQS FIFO, you need to provide a field called MessageDeduplicationId.
And, as you can expect, that ID should be unique for that message, and if you send it twice, then it will deduplicate on it.
So how does that deduplication work? Well, there's a deduplication interval that is five minutes.
And so, if SQS, within that five-minute intervals, sees the same message with the same message duplication ID, then it will obviously deduplicate
and discard the other message. There's also this feature called content-based deduplication, and that means that
the MessageDeduplicationId will be generated as the SHA-256 which is a hashing algorithm of the message body, not the attributes.
So using content-based deduplication, that means that if you send twice the same message with the same body, then the second message
will be deactivated. So, content-based deduplication is also very handy if you just want to deduplicate on the payload
and you don't have an ID in mind. The other thing to think about is sequencing. So, to ensure strict ordering between messages,
you need to specify a MessageGroupId. And messages that have a different Group ID may be received out of order. So what does that mean?
Let's take an example. If you wanted to order all the messages for a single user, you could use the User ID as the Group ID.
And that means that the message with the same Group ID will be delivered to the same consumer and will be processed with strict ordering.
So when you think about SQS FIFO, the ordering comes from the MessageGroupId. Two messages that share the same MessageGroupId
will be received in order, whereas two messages that have different MessageGroupId may be received out of order but may also be consumed
by multiple consumers at a time. So, we'll be seeing deduplication and sequencing right now in the hands-on, so don't worry if you don't
understand these concepts just yet. Hopefully, the hands-on will help. So let's get into it. So let's create a new queue,
and this time it will be a FIFO queue. And for the queue name, we'll call it MySecondQueue. And as you can see, there is a message saying
it needs to end with .fifo. So, .fifo. Okay, we scroll down. As we can see, the FIFO queue has 300 messages per second
and can batch, if you use batching of 10 messages per second you can reach 3000 messages per second. So, in terms of features,
FIFO delivery exactly once processing, and you can see here the messages are read in order and sent in order.
So if you scroll down, we're just going to do Quick Create Queue, and not configurate any further. So this is our FIFO queue, and as you can see,
the queue's type is .fifo in the UI, and for now content-based deduplication is disabled, but we'll see how to enable it later.
So I need to right click and send a message, and let's send a message. So we'll send hello world. And in the bottom, we need to specify both
the Message Group ID and the the Message Deduplication ID. So, for this Message Group ID, I'm just gonna say
mygroup for now, and let's look at Message Deduplication ID. For example, say this message has the ID 123,
and we're going to send it. So I've just sent my message, and here we get back some sequence number and so on.
If I close it, I can look at the number of messages available, there's one. Now let's try to send the second message.
We'll send another content, right? And the Message Group ID is going to be the same, mygroup. And the Message Deduplication ID
is going to be the same, 123. So in this case, the content of the message is difference, the Message Deduplication ID is the same,
so if we send the message, it's going to be deduplicated because we have sent the same deduplication id, and the number of messages available
is still one. And if I send another message, let's send Hello World, and my Group ID is going to be
mygroup, and the Deduplication ID is going to be 234. So this time, we've sent a different Deduplication ID
and the same body as one of the messages from before. And we send the message. This time, it's going to be accepted
because the Deduplication ID is something that SQS hasn't seen yet. So here, we just demonstrated the impact of
the Message Deduplication ID. You could go as well and configure the queue. So Queue Actions, and then Configure the Queue,
and we could enable Content-Based Deduplication, and it says it will use a SHA-256 hash in the body of the message
to generate the Message Deduplication ID. So if we try that out, save the changes. Now that means that if we send a message, um,
Hello World again, and we'll say in mygroup. Now we don't have to specify Message Deduplication ID because it will be generated for us.
So let's send Hello World again, and that message should have been accepted. So yes, we have three messages available.
And then, if we send Hello World again in the same mygroup and send the message again. Now because we have Content-Based Deduplication
enabled, that message should not be accepted. Let's have a look. Yes, indeed it was not accepted because we still have three messages available.
So that proves it. Now, let me just, ah, go back and send a message. So now we've seen deduplication id's,
and we've seen content-based deduplication, so a way that is clear. Now, let's talk about Message Group ID.
So, it's not something I can directly demonstrate for this UI to prove the ordering, but the idea is that if I send an information
for people user bought an apple, and the Message Group ID is going to be userid_1234. If I keep on sending messages
with the same User ID, they will be in order for that user. So let's have a look. I'll send a message.
I'll send another message, and then I'll say, "User bought an apple," and then "User checked out,". And send a message.
Now, these two messages share the same User ID, 1234, the Message Group ID, therefore they will be read in order.
We can just look at receiving messages, so we can just view and delete message, and we start pulling for message,
and here we can see all the messages that were sent to our FIFO queue. For example, the message group.
And as we can see, they're sent, they're received in order for that one Message Group ID. Same for the User ID's.
So if we look at it, there is "user bought an apple", and "user checked out", and they show the same User ID,
and obviously we sent "bought an apple" first and "user checked out" second, so we can see everything.
As well, we can see the Message Deduplication ID that we sent, 123, 234, and as soon as we enabled Content-Based Hashing and Deduplication, then
the Message Deduplication ID get generated by SQS itself. So that's it, that's all you need to know about SQS.
But remember these very important concepts around ordering, around the Message Group ID and the Message Deduplication ID,
because these come up on the exam. All right, hope that was helpful, I will see you in the next lecture.
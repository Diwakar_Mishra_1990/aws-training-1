So that's it for the extended client. Now let's talk about security, 'cause security's always important.
There is in flight security using the HTTPS endpoint. Again any time there is an S to HTTP
that means SSL, that means secure, that mean encrypted. We can also enable SSE for Server Side Encryption
and that leverages KMS. So KMS we can set our own customer master key we want to use and we can set something called
the data key reuse period. That's a bit more advanced but basically the lower you set
this period the more KMS API will be used and you will have to pay more. If you set it to higher then you have slightly less security
but you use the KMS API less and so you control your costs. So you can set the data key reuse period
between one minute and 24 hours, and by default it's five minutes. SSE will only encrypt the message body not the metadata,
so not the message ID, the timestamps and the attributes. So don't ever store sensitive information
into your attributes, just in the body. Now the IAM policy must allow the usage of SQS,
but we know this by now. IAM basically controls all the API calls done within your AWS accounts and so it's very important
to allow the usage of SQS. And on top of it there is this obscure concept, a bit more advanced, call the SQS access policy.
Basically it's a finer grained control, you can use the IP for deciding who wants to access and you can say when the messages come in
and from which accounts. It's a little bit more advanced, I don't think you need to know it for the exam.
Finally, very common question is, there is no VPC endpoint and so you must have internet access to access SQS.
So you can't access SQS privately within your VPC for now. Now for SQS finally when you go into the exam
there is a few API you must know, but we've seen them already. Let's just see them one more time. There is the CreateQueue and the DeleteQueue,
they do what you expect, they create a queue and they delete a queue. PurgeQueue to delete all the messages in your queue
and this takes up to 60 seconds to happen. SendMessage, ReceiveMessage and DeleteMessage, they do what they basically indicate,
you send one message, you receive one message and you delete one message. And you can change the message visibility
so you can change the timeout of a message to increase it in case you need a little bit more time to process your messages.
There is a Batch API for SendMessage, DeleteMessage and ChangeMessageVisibility and so that helps to decrease your costs.
So there is Bach SendMessage, Batch DeleteMessage and Batch ChangeMessageVisibility. Just remember that ReceiveMessage can receive
up to 10 messages and that's why there is no Batch ReceiveMessage. So you need to know these APIs and you'll be good.
So that's it for the advanced SQS, just a little bit of overview into what else you need to know,
but by now you should be an SQS master. And I hope that was helpful and really put things into perspective.
Good luck at it for the exam and I will see you in the next lecture.
Using Amazon S3, it is possible to set up a static website, and for it to be accessible to anyone on the web.
The website URL will be of two forms, there is this one, or that one. To be honest, to catch the difference is a bit tricky,
but after s3-website, they can either be a comma like here, or it can be a hyphen right here.
It really depends on the region you're on, but overall, it looks like
    <bucket-name>.s3-website-<AWS-region>.amazonaws.com
Okay? Sometimes you may be asked what is the right URL, I don't think it's going to be the case now, but,
you never know. If you get a 403 Forbidden error when you set up the website you need to make sure that the bucket policies
allow public reads. And we will get that error in the hands on and we'll see how to fix this
using the bucket policy. So let's go ahead and learn how to set up a website on AWS. Okay, so first things first,
let's see what we're going to do. We are going to create an index of HMTL file, and I have created it right here,
it is a very simple HTML file, in which I will say, I love coffee, hello world, and display the image of a coffee.
Pretty simple right? So if we go to our directory, this is just on my local computer,
and I click on the index.html, I see this page. As you can see right here, the URL is not a URL that is public,
it's just on my computer, But this is what we want to see at the end of this tutorial. Okay, now that we've seen what we want to see,
let's see how we can make it work on Amazon S3 static websites.The first thing you have to do is
go back to permissions, bucket policy, and make sure I delete this policy entirely,
so now we have no more policy, this way we're back to the default. Okay, so now I go to overview, and I can click on upload
and add a file, and I will add my index.html file. I will click on next, next will leave encryption to be equal to none,
click on next and upload. As you can see, it did not work,I get an error on my upload of my file of index.html.
And the reason is when you do edit bucket policies, when you do change your bucket policy, it can take a little bit of time
to be reflected in your bucket. That's Amazon S3, It takes sometimes a bit of time. So let's go again and try to upload this index.html file,
but I want to keep this error just to show you that sometimes when you change your bucket policy
the result of it isn't instantaneous. So this is my third try, but I waited five minutes, and it basically took five minutes for it to be active.
But now I can upload my index.html just fine, it just took five minutes for the bucket policy to replicate.
Okay, so index.html is fine and if I click on it, we can see that it works, it has no encryption, and it is there on my bucket.
If I click on open, it doesn't work, right. We still get like a website, a URL, but it's not able to access the image.
We'll see how to fix this right now, for this we go to properties and we click on Static Website Hosting.
Out of it, I will say use this bucket to host a website. The index document is index.html, the error document is fine, and then we click on save.
And as you can see, out of it, if we click back on this, we get an endpoint right here that is the full endpoint of my wesbite.
So if I click on this endpoint, I get a 403 forbidden, access denied. What does that mean?
That means that basically, I am not able to access my bucket because it doesn't have the right policy.
So I have to add a bucket policy, which will allow anyone to read content on my bucket. For this I go back to my console.
I will go to permissions and now since November 15 we have to click on public access setting and this
is basically some settings that AWS put so that we don't put stuff in public mode without knowing that
we actually do it. So it's a safety to make sure there's no data leaks. And so just for these tutorials sake we have to untick the last two ticks which is block new public
policies and blocked public and cross account access if the bucket has public policies. And the idea is that because we're going to apply a bucket policy right now that is going to grant public
access to the bucket because it's a Web site we have to untick these two things. But overall this is a really good protection.
So click on save then we have to type confirm and then click on confirm. And now we have the public access setting successfully updated and now we can click on bucket policy.
And here I need add a bucket policy. Let's use again, the policy generator, for a type of policy, S3, we'll allow Principal star, so anyone to
use amazon S3 for the action get object, so we want to allow anyone to read our objects,
OK.
and the ARN, I need to get from here the ARN of my buckets. So I'll copy this, paste it, slash, star to only
allow objects. Click on add statement and generate policy, and here is our policy, which should allow read access to our bucket as public.
I'll save it. And if everything works we get this warning saying this bucket has public access. So basically now anyone can read any files in my bucket even you can.
If I leave this bucket on and so basically it says you recommend that you never grants any kind of public
access to your Amazon S3 bucket unless you're using an Amazon S3 website. So that's it.
We go through overview and know basically if we refresh these Web sites we get.
I love coffee, Hello world, and now this works. So we passed a 403 forbidden.
As we can see, it loaded my HTML page, and it also loaded my image coming straight out of my bucket.
So it's pretty cool. You can also test directly from the image you can click on the image and click on the link and this
will directly load the image in a big file in a big screen. So this is this is how you set up a static web site in Amazon.
Just remember that you have to basically set up the permissions and make sure the bucket is public.
Using this policy which allows anyone to get objects of your bucket and then you need in properties
to make sure you enable a static web site hosting.
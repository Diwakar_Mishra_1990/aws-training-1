Okay, now let's talk about S3 security. So, there's two types of security. There's gonna be user-based and resource-based.
For user-based, you are going to use IAM policies. And we haven't seen exactly how IAM policies work,
but, basically, through IAM we can restrict what the user will do against Amazon S3. Now, the more popular way
to actually handle Amazon S3 security is called resource-based. And for resource-based, you have something called bucket policies,
which is by far the most popular type of policy you can put on your bucket. And so it's a bucket-wide rule from the S3 console,
and you can use it to allow cross accounts, et cetera, et cetera. We'll see this in the next slide.
You can also set Object Access Control List, or ACL, which is a bit finer grain than the bucket policies,
but less popular. And the Bucket ACLs, which are even less common. So, overall, there's just four different ways
to manage S3 security. What the exam expects you to know, really, is IAM policies and bucket policies.
IAM policies we'll see in a future lecture, but bucket policies, this is for this lecture.
So, for the bucket policy, basically it is a JSON based policy or document. And it will have four major elements:
the resources, which is the buckets and the objects the policy will apply to; the action, which is a set of API that will allow or deny;
the effect, which is actually allow or deny; and the principal, which is the account or the user to apply the policy to.
So, using these four things, we can do a lot of things. We can use the S3 bucket policy to do a bunch of things,
such as grant public access to the bucket, or force objects to be encrypted at upload, or grant access to another account,
which is also called cross account access. So we'll practice all these. We'll grant public access to the bucket in the next lecture.
We'll force objects to be encrypted at upload in this lecture. And cross accounts is left for your own practice,
but it is quite easy if you Google it. Other things you should know about S3 security that can come up in the exam,
number one is about networking. S3 supports something called VPC endpoints. That means that your instances in you VPC
that don't have Internet access could be talking to S3 directly through the internal network. There's also logging and audit features,
so you can have S3 access logs, and you can store these S3 access logs in another S3 bucket. Now, something you should not do
is store your own access logs in the same bucket the access is being logged, because, otherwise,
there'll be, like, some recursion and the access logs will just go skyrocket, and you'll end up with 50 or 100 gigabytes of files.
The API calls that you do on S3 can be logged in AWS CloudTrail. We haven't seen what CloudTrail is,
but it basically allows you to audit your entire API calls stack in AWS. And, for user security, you can enable MFA,
or multi-factor authentication, just to make sure that users need to provide a certain ticket if they want to
delete an object in a version bucket. And, finally, very popular as well at the exam, you can use signed URLs,
which are URLs that are only valid for a limited time. For example, if someone is a logged in user
and they want access to a premium video, then you can generate a signed URL just for that user so they can view the video.
So it's also quite a common technique at the exam. They will say, "Oh, how can we grant access
"to one user for a little bit of time and then expire it?" The answer is use S3 signed URLs.
So, in the hands-on we're gonna do right now, we're getting to practice bucket policies. So let's go ahead and add a bucket policy
that is quite common and quite popular at the exam, which is how to ensure that the objects can be encrypted at upload.
So, in the properties, I'm going to disable the default encryption. Click on none. And so, basically, what we want to achieve
is that we want to make sure that the bucket will say no to us if we upload a image that is not encrypted.
Okay, so, to do this, we have to go to permissions. And, as you can see in here, there is the access control list,
which are quite uncommon and not very popular at the exam, and the bucket policy that are extremely popular.
So I click on bucket policy, and we arrive in the bucket policy editor. So here we can have to type a JSON document.
And so there is some documentation you can look at, or there is a policy generator. And I really, really like the policy generator,
so let's click on this and use it. The type of policy we will create is S3 bucket policy. And this tool will, basically,
allow us to write that JSON document all from this UI. I find this quite nice and a lot of people use it.
So we want to make sure that we're going to deny two statements. And these two statements, at a high level,
one will be we deny any request that does not contain the header for encryption, and then the other one is we deny any request
that does not contain the right value for the header for the encryption. So let's go ahead and do this.
It's gonna be tricky, but you do deny, so effect, deny. Principal is going to be star because it applies to everyone.
The service is Amazon S3. And the action is PutObject, so you have to scroll down and PutObject.
Now you're supposed to specify an ARN and say, "This policy applies to." And we go back to our S3 management console.
We have to copy this entire block right here, the ARN. And you make sure to add slash star, okay.
Now we add a condition and say, "Okay, we deny anyone who does a PutObject, "okay, on any file, on any object within my bucket if."
And here we select the condition no. The key is going to be, basically, the header for encryption,
which is the server-side, AMZ server-side encryption, and the value of it will be true.
So, basically, what this condition does, and we click on add condition, is saying, "If the AMZ server-side encryption header is no,
"okay, then we have to deny it." So that means forcing people to add the header. So we add statements, and that's number one.
And the second statement we need to add and say deny. Principal is going to be star. Service is Amazon S3.
Action is, again, PutObject, which is right here. The ARN is the same, slash star. And this one we add a condition,
and we say, "When the string not equals," we scroll all the way down to AMZ server-side encryption and we say AES 256.
Add condition. So what this does is, basically, anytime someone does a request with this header,
this header is not equal to AES 256, then we're going to deny it. So this forces server-side encryption
to be equal to this value, so which is SSE-S3. We add a statement, so we have two statements here.
And now we click on generate policy. So this generates the very complicated JSON for us, so this is nice.
We go ahead and copy this, and then paste it. By the way, I didn't come up with all this, and you shouldn't remember all these things.
Okay, so let's just click on save. This bucket policy is something you can do using SS3, SSE-S3 bucket policy.
And, by Googling it, usually, you get a first link on the Amazon documentation, which shows you exactly how to have the nice policy.
Okay, so you can just copy it from here. But it's nice to be able to practice and generate it using the AWS policy generator website.
So we have this bucket policy, and it's just saying forcing encryption. So let's save this.
It's saved. And now let's test it out. So I'm going to upload a file. I'm going to add a file,
and this file's going to be my coffee file. And I'll click on next, next, and I'll set encryption none.
Click on next and upload, and now I get an error. As you can see, the upload has failed.
And the thing has failed because it says, "Forbidden." Okay, I am not able to upload this
because my bucket policy blocked it. Now, if I upload my file, again this coffee file right here.
Click on next, next. And this time I say Amazon S3 master-key, this should set the right header.
Click on next, and upload. This one time, it is successful, so now we get the successful request.
Finally, let's have a last test. We'll add a file. We'll add again the coffee file, and this time I click on next, next,
and I'll choose AWS KMS master-key. I'll select a random key, so AWS S3. Click on next. So we are doing server-side encryption,
but we're not doing the right one. I click on upload, and, as we can see we get another error, failed,
because we haven't specified the right encryption policy. So using the bucket policy is a very, very good way
to ensure that objects are going to be encrypted when they get put into the bucket.
Otherwise, the upload will fail. And so that's a very popular question for the exam.
You should know you choose a bucket policy if you want to encrypt files on your S3 bucket,
but I wanted to show you through hands-on how this thing worked at the very low level
so you get a better idea of how things work. Hope that was helpful, and I will see you in the next lecture.
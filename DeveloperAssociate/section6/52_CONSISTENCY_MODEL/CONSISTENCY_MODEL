So, something you should absolutely know about S3 is the consistency model. So as you can see in before,
basically when we do something in S3 it can take a bit of time to replicate and to be active.
That's because S3 is a global service and things can be slow and eventually consistent.
So there are two types of consistency you get out of S3. The first one is the read after write consistency
for PUTS of new object. So when we use the PUT object API and the object did not exist before
we will get read after write consistency. That means that as soon as the object is written we can retrieve it.
So if we do a PUTS, we get a 200 that means okay and then we can do a GET on the exact same file
and we'll get a 200 that also means okay. So just know that if you write a new object
you can retrieve it right away. This is true only if you did not do a GET before to see if the object existed.
So if you have an application you first do a GET to see if the object exists. You get a 404.
Then you do a PUT because the object doesn't exist so you put the object in the S3 bucket and then you do a GET again.
You may get a 404 because you are eventually consistent. That means that this time, basically,
the previous result was caged, which was 404, and so you need to wait a little bit, that's why it's called eventually consistent,
if you want to have a GET 200. So you also get this eventual consistency when you do DELETES and PUTS over existing objects.
That means that if you reading objects after updating it you might get the older version. So if we do a PUTS 200,
so that means the first time we upload an object, then we do another PUT 200 so you upload a new version
of the object, and then we do a GET, we'll get a 200 back but it might be for the first version
not for the second one. That's because it's eventually consistent. And then finally if you delete an object you might
still be able to retrieve it for a short time afterwards. Again, because it's eventually consistent.
So if you do DELETES and you get a 200 on that object that means okay.
And then you do a GETS you might get a 200 instead of a 404. That's eventual consistency.
So it's quite, quite popular question at the exam. What you need to know is that for new objects
as soon as you write it you can retrieve it. So read after write consistency for PUTS
and then if you override an object or delete an object you get eventual consistency.
That means that you basically might get an older version or might even get the object after it's been deleted.
Just for a short time. For the time basically for AWS to replicate your objects on their cloud in S3.
So I hope that helps around what the consistency model means. Just remember two things:
read after write and eventual consistency. Okay, I'll see you in the next lecture.
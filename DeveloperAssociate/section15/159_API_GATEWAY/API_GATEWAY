So let's talk about API Gateway. API Gateway is a way for us to build, deploy, and manage APIs,
and APIs are interfaces we can expose to other people within our company or in the outside world.
So the goal of this whole serverless thing really is to deploy APIs and so we've seen from before that
we have AWS Lambda as a compute source that can automatically scale, that is serverless,
and that runs function as a service, but we also saw in the past section that we can use DynamoDB as a serverless database,
provision tables, and interact directly with AWS Lambda, for example, to do operations such as create, read, update, and delete.
So we have a computing side and we have a serverless database side. Now, how about the clients?
Well the clients needs to interface and with our Lambda and our DynamoDB and the client is our application.
I would love to have is something in between and that something is Amazon API Gateway that will take the client request as REST API
and then translate those and pass them on to the Lambda functions. The reason we have an API Gateway is
that we can have more control over what the client can and cannot do and we can defer to business logic
in how these requests should be handled to AWS Lambda. So overall, it makes sense. API Gateway is going to have an integration
with Amazon Lambda and overall that gives you no infrastructure to manage and that gives you a serverless REST API.
AWS API Gateway is great because you can handle versioning. So if your API evolves from v1 to v2 you can do that with API Gateway.
You can also handle different environments. So if we have dev environment, test environment, prod environment, all of these can exist
in our API Gateway and we can manage deployments. API Gateway will also have tight integration with security.
So we can remove the security concerns out of our AWS Lambda function and have them taken all the way through
to the API Gateway and we'll see in great details how the API Gateway integrates security with other components.
API Gateway is great because also we can create API keys, and we can handle request throttling
if we're selling our API to people, and it has a very tight and neat integration with something called Swagger/Open API,
and that allows us to import some files, and define quickly our APIs, and even export them as SDKs for example.
API Gateway can also help us transform and validate requests and responses, and, as I said, generate SDK and API specification.
Finally, it also has an embedded caching layer you can implement so you can cache your API responses and limit the load that happens
on your Lambda function. In terms of API Gateway integration, it has a lot of them. You have to separate them between
outside your VPC and inside your VPC. So outside your VPC you can run AWS Lambda function, and that's, by the way, the most popular and powerful
type of combination, but also EC2 instances, load balancers, any AWS service, as a matter of fact,
and you can even proxy the request from API Gateway all the way to a HTTP endpoint that you may control.
You can have a front layer to your existing HTTP APIs. Now inside of VPC you can enable, again,
Lambda integration when Lambda is the pod within your PC, and also EC2 endpoints in your VPC. So that's it for the API Gateway integrations
and a quick overview. I think API Gateway overall is a huge service and very complicated to learn
and master overall so this is going to be an overview, but in the next lecture I wanted to go straight on,
hands on, so we can get an idea of how API Gateway works, 'cause it probably will be much clearer
than a bunch of text on slides. So see you in the next lecture.
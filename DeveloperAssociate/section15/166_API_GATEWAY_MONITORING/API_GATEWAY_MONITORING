Now the really cool thing with API Gateway is that you can have logging, monitoring, and tracing.
So it's integrated with CloudWatch logs and we just need to enable it. But we need to enable Cloudwatch logging at the stage level
and then we can override it on a per API basis if we wanted error logs, debug logs and info logs etcetera.
The log can contain information about the request and response body, which is super helpful if you want to debug some things.
Also we have CloudWatch metrics to see how each stage responds, if there's any errors, and we can enable detailed metrics to get even more metrics.
And then the really cool thing is that now there is X-Ray integration. So you can enable tracing at the API Gateway level
to get extra information about requests made from the API Gateway to your lambda function, to your DynamoDB baby for example.
And so it's a really powerful integration the API gatewas plus the AWS Lambda tracing  that gives you really the full picture about
how your requests go through your entire system. So let's have a look at how we can enable these. So in my dev stage I'm able to go to logs/tracing
and enable CloudWatch logs, access logging, X-ray tracing and so on. So for CloudWatch logs I'll say okay
I want to enable it and I want to have info log level and I want to log full requests/responses data.
This is a bit tricky if you have sensitive information in your request to response so be careful with this option
and then maybe you want to have detailed metrics, access loggings or X-Ray tracing. Now I can go and click on save changes but it won't work.
The thing is I first need to go into settings in my API gateway and I need to provide a CloudWatch log ARN for my API gateway.
So I need to go into IAM, let's do this together, let's go to IAM and I create a role, I'll create a new role
and this is for an AWS service which is API gateway, so this is my use case and this allows API gateway
to push logs to CloudWatch logs. Click on next permissions, I will choose amazon API gateway push to CloudWatch logs,
already knows what to do. Oops, I'll just click on this policy click on Next:Review. The role name is called APIGateway and click on create role.
Now we have the role APIGateway that has been created, we can copy the role ARN using this copy button
and we can paste it right here. So now we are in the settings, we're providing API Gateway with a CloudWatch log ARN.
Click on save and now API Gateway can send logs to CloudWatch. Let's go back to our PetStore, Stages,
click on dev, logs/tracing now we can click on enable log for CloudWatch, info and enable X-Ray tracing
and we'll log the full request and response data, click on save changes and now my stage is fully integrated with logs.
I'll just refresh the page to make sure and if I go back to logs/tracing, yes everything is still ticked so it's working.
Now I go back to my Invoke URL and I'm going to invoke maybe the slash pets API, refresh this a few times and so what we expect is
that if we go to services and we go to CloudWatch, from CloudWatch we're going to logs on the left hand side
we can get API gateway execution log and we have an API gateway welcome so let's just refresh this real quick.
Okay let's have a look at what we want to have. Maybe this execution log is what we're looking into
and so we have a lot of log streams available for us already but here by clicking on this we can see that we have
the slash pets resource that was requested and then it gives us all the information and logs we get,
including the response body before transformation and so on. We get a lot of different information in CloudWatch,
now if we want to do something fun and go to X-Ray. So I'll type on X-Ray and see if in my service map
I see my API gateway. And yes, in my API gateway I can see that my clients has stopped in my API gateway stage slash dev
and what happened is that it went and talked to my pet store demo remote endpoint HTTP. And we can see the response time of this endpoint
versus the API gateway. Which is kind of cool because now we can see that the API gateway has given me a lot of information
on what my APIs do. We can do something else just for fun and go to MyFirstAPI, stages, dev, logs tracing
and I'll also enable X-Ray tracing here just for fun. Now I will invoke my dev API so I'll go to dev
and I will also go to slash dev slash houses just so you have two different APIs being hit and that's good.
Now if we go to X-Ray and we refresh that service map. So after a few minutes of refreshing,
I actually can see a lot of cool information now. In my API gateway I can see that my dev stage has different API lambda functions it can call
so there's this root get or houses get and then we can see how each lambda function calls it's own little lambda function
so this is lambda service and this is lambda function so here we're really able to see the whole service map
through X-Ray of how our APIs behave. Obviously if this lambda functions were talking to DynamoDB we would see DynamoDB here
and that makes things really cool. If we go to traces and click on for example: dev houses and click on this URL,
right here we're able to see all the trace list. I'll click on one of them and now we're able to see exactly
the breakdown of the latency. The first API call took 20 milliseconds, but 15 milliseconds were spent on Lambda,
eighth milliseconds on that service, one millisecond on executing the the function and so on.
So it's really cool I think to have X-Ray integration with API gateway and AWS Lambda, because we can start seeing a lot of different patterns
which I think are quite useful when you start having very big applications. So I hope that was helpful for monitoring
and logging and I will see you in the next lecture.
Okay, so let's get started with Lambda. So I'm going to type Lambda in my search bar and I'm going to get the Lambda console.
So you may be on this screen or you may be on another screen and actually want to get to the other screen,
so I haven't found a way to get there consistently using click options, so I'm just going to type begin in the URL.
So my region, pound slash begin, and press Enter, and this takes me to this nice screen, which I think is good for educative purposes.
Nothing you need to do for getting the hands-on, but I just want to show you how Lambda works before we write our first Lambda function.
So Lambda is a serverless service on AWS where you can only pay for what you're going to use in terms of compute usage.
And so there's no charge when your code is not running. So there is a how it works box that I really like here,
which is, okay, here is our JavaScript code, and this is a function, and the function is just returning the word hello world.
And so if we run this, and this is a Lambda function, the Lambda function has just run, and now we have the result hello world.
This is very, very simple, but we can have a lot more complicated Lambda functions in there that just do a lot more
than just saying hello world, okay? So next, if we look at Lambda, we can see that Lambda responds to events that happen.
So here's my Lambda function, and currently it's being invoked by streaming analytics. And so we can see in this little log box in here
it just keeps on saying hello world. And this demo is quite interesting because we can see from here
that the data can come from data processing, so in S3 buckets. It can come from photography. It can come from streaming analytics.
It can come from mobile and IoT devices. So if I click on the mobile, then it goes and sends a message to the back end
which goes straight into Lambda. And so if you can see, the more I click on these things, the more there are cog wheels on the right-hand side
which show that many Lambda functions are being invoked. And as there's less data and less invocations of Lambda being sent, then there's less cog wheels.
So the Lambda function, if I click a lot on this mobile device, as you can see, scales accordingly to what it looks like.
And if I click on scale seamlessly, now we can look at the invocation and the cost. So there are very little invocations.
As you can see, the cost is almost zero. And as I start invoking more and more my Lambda function, the number of invocations goes up,
the concurrency goes up, and then the cost starts going up, as well. But as we can see for this example, for example,
I have 4 million invocations and a cost of $3, so it is really a little amount of money. So this is the whole power of Lambda.
Here with AWS Lambda, we can see that we scale based on the load that we have. We can integrate with many different back ends,
and the more we send events, the more we have to pay, and only that. So the cost is really linked to the number of invocations
and how long these invocations lasted. So just a cool demo, but I think it's good for educational purposes. And so next we click on create a function,
and if you had to have on this screen, then you can just click on create function right here and you get to the exact same screen.
Okay, so we're all in the same place now, hopefully. And so to create a function, we have three options.
We can author it from scratch, and it starts with a simple hello world example. We can use a blueprint,
and this will use some kind of pre-defined Lambda function for us, or we can browse a serverless app repository
which contains a bunch of apps created by public people and so on. So in here, I'm just going to use a blueprint,
but we could also author from scratch. And so in the blueprint, I'm just going to type the word hello.
And this takes me to two hello world. There is hello-world and hello-world-python. And so I'm going to click on this hello-world-python.
For now it's Python 3.7, but maybe in the future it'll be Python 3.8 or something like this.
It doesn't change anything, okay? Just click on hello-world-python. And you don't need to know Python in advance
to understand what I'm going to do. It's just that I think it's a language easy to read and understand.
So click on hello-world-python and then click on Configure, and we're getting to the blueprint configuration.
So this function, I'll name it hello-world, and then I have to choose an execution role. Now, a role is a IAM role
that will be attached to our Lambda function that will allow us to execute stuff for our Lambda function
and tell us what it can and cannot do, for example, for interacting with EC2 or S3 or whatever. So for this role,
I'm going to create a new role with basic Lambda permissions, and this role will be created for me,
and what it will come is that it will come with permissions to upload the logs to Amazon CloudWatch Logs.
So this is a basic permission role for Lambda which allows Lambda to just log what it does into CloudWatch.
So this makes sense. And the role creation will take a few minutes, and then the role will be named something different for you,
but for me it'll be hello-world-role and then this blob of text in here, okay? Now the code itself, it's a blueprint,
so I'm not going to modify it, but here is a very simple Lambda function in Python where we have a function,
which is a Lambda handler, and what it does is it prints a lot of stuff to the console. So value1 equals event key1,
value2 equals event key2, and so on. And then finally, the result it returns is event key1 to echo back the first key value.
It's just a very simple function which doesn't do anything fascinating, but it just shows that Lambda functions
can execute code that's arbitrary. So we'll click on create function and get going. So now as we can see,
the function has been created, and we can test it. So I'll click on Test and then I'll create a new test event.
I'll call it from the Hello World template and I'll call it SimpleEvent. And this will contain just a JSON document
with key1, value1, key2, value2, and key3, value3, and this will be the event will use to invoke our Lambda function.
So I'll click on Create, and now the event has been created. So now if I click on Test, I should be able to test my Lambda function,
and the execution result is succeeded. So if I look into the details, it shows that the result is value1,
and if I scroll down, it shows the log output of the function that happened while it was executing.
So it says value1 equals value1, value2 equals value2, and so on, which is the output of the function code
that is in here in the bottom. So as we can see, other things that are interesting is that the duration is 1.54 milliseconds,
so it was really quick to execute, but I've been billed 100 milliseconds because we get billed in 100 milliseconds increment
in Lambda for now. In terms of the resource configured, 128 is the default, so this is what has been configured for this blueprint.
And the actual memory usage has been 56 megabytes, so I know how I'm doing versus the memory I've provisioned
and the memory I actually used, okay? So this is really cool. We can also go and click here to view the logs
in CloudWatch log group. So if I go into CloudWatch in here, I'm able to find this log group called /aws/lambda/hello-world,
and there's a log stream, and in this log stream, I'm able to find the exact same log that was in here
in this window back into CloudWatch logs, okay? So this is excellent. And then what else can I show you?
If I scroll down, I'm able to modify the code if it's very simple code. So in here what I can do is instead of returning event key1,
I need to comment this out and I uncomment the one that says raise exception, something went wrong.
So I'm going to save my function in here, and then after saving my function, I can test it again.
So I'll test it. And this time the execution result has failed and so we have the error message something went wrong,
and we have the stack trace and so on, and we can look at the log output, as well, and see what happened.
So the really cool thing here is that we are also able to see this log output in CloudWatch logs. So if I click on this execution right here,
I should be able to see the exact exception that happened, and so on. So this is really helpful for debugging, okay?
So this is it just for a quick overview of Lambda. Obviously we'll do so much more with it. But before we move on to the next lecture,
I'm going to uncomment the good code and comment the code that raises an exception and then click on test,
save and then click on test. As we can see, everything is green, so it succeeded. So that's it just for an intro to Lambda,
and the last thing I want to show you is that if we scroll down, we can see that this hello-world Lambda function
is linked to CloudWatch logs. So this is because we have given our Lambda function an IAM role to get access to CloudWatch logs, okay?
And so there's a link here called manage these permissions, so this is, it's saying why we have these permissions.
So this takes me straight into the IAM management console. And so if I went back into the roles, by the way,
and just typed hello-world and searched for it, I should see the hello-world, this one, the hello-world-role and with the blob of text.
So this is a Lambda function IAM role, and there's been a basic permission applied to it which is a managed policy
which is the AWS Lambda basic execution role, and this policy allows two things. It allows to create a log group,
so create a log group for the CloudWatch logs, which allowed it to create this log group right here,
and the second thing it allows is to create log stream and put log events to the log groups that it created,
and this is great because now we are able to see the logs in our Lambda function in here
thanks to these IAM permissions. And this is the only IAM permissions your Lambda function needs to log its stuff to CloudWatch.
And this is why, in this UI, you can see that the Lambda function is linked to Amazon CloudWatch Logs, okay?
Well, that's it for this lecture. I hope you liked it, and I will see you in the next lecture.
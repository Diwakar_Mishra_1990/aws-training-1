Now we're getting an AWS ElastiCache overview. So the same way RDS is to get a managed relational database
ElastiCache is to get a managed cache, in this case, Redis or Mamcached. And caches are basically in memory databases,
so they run on RAM and they have really high performance and usually a really really low latency.
And their role is to help reduce the load off of databases by caching data so that they read intensive workloads.
They read from the cache instead of reading from the database. So basically it also helps make
a bunch of applications stateless by storing states in a common cache. And it has a Write Scaling capability using sharding.
It has a Read Scaling capability using Read Replicas. It has Multi AZ capability with Failover, so just like RDS,
and it has AWS taking care of OS maintenance, patching, optimizations, setup, configuration, monitoring,
failure recovery and backups. So basically, it looks a lot like RDS, and there's very good reason. It is pretty much the exact same thing.
It's an RDS for caches and it's called ElastiCache. Okay, that's what you should remember. So there's Write Scaling, Read Scaling, and Multi AZ.
Now, you may be asking, "How does it fit "Into my solution architecture?" And at first I was troubled too and really,
this diagram that I created really helps put things into perspective. So, when we have our application,
it communicates to RDS, as we've seen from before. But, we're also going to include an ElastiCache.
And so our application will basically first query ElastiCache. And if what we query for is not available,
then we'll get it from RDS and store it in ElastiCache So that's called a cache hit, when you get into ElastiCache and it works.
So we have application, it does a cache hit, and we get the data straight from ElastiCache.
In that case, the retrieval was super quick, super fast, and RDS did not see a thing. But sometimes our application requests data
and it doesn't exist. This way, it's a cache miss. So when we get a cache miss, what needs to happen,
our application needs to go ahead and query to database, so we'll go ahead and query to DB and RDS will give us the answer
and our application should be programmed so that it writes back to the cache the results in ElastiCache.
And the idea is that if another application, or the same application will ask for the same query,
well this time it will be a cache hit. So that's what a cache does, it just caches data. So the caches will help relieve the load in RDS,
usually the read load, definitely, and the cache must also come with an invalidation strategy,
but that's up to your application to think about it, so that only the most current data and most relevant data is in your cache.
Now another solution architecture is User Session Store. And so in this case our user basically login to our application
and our application is stateless so that means there is a bunch of applications running, maybe they're running into an autoscaling group
and so all of them need to know that the user is logged in. So the process is such, the user logs into the application
and then the application will write the session data into ElastiCache. So this is it, the application, the first one,
just wrote the session data into ElastiCache. Now the user hits another instance of our application
in our autoscaling group, for example, then that application needs to know that our user is logged in.
And for this, it's going to retrieve the session off of Amazon ElastiCache and say "Oh yes, it exists"
so the user is logged in. And basically all the instances can retrieve this data and make sure the user doesn't have
to re-authenticate every time. So that's, basically, another very common solution architecture
and patent with ElastiCache. It is basically, number one, to relieve load off the database
and number two, to share some states, such as a user session store, into a common ground, such as all the applications can be stateless
and retrieve and write these sessions in real time. So as I said, ElastiCache can be basically two technologies.
It can be Redis or Memcached. And so Redis is the most popular. It is an in-memory key-value store
and it is super low latency. So we're talking sub millisecond, so less than a millisecond latency.
The cache can survive the reboots by default, it's called persistence, so not only it's a cache,
but if the cache reboots, we don't lose your data, and it's pretty amazing. It's great to host user sessions, as we've seen before.
Also leaderboard for gaming because there's a sorting capability. Distributed states, so again the user session is a good one.
Relieve pressure on databases such as RDS and we saw this in the architecture slide. It also has a Pub/Sub capability for messaging
if you want to implement Pub/Sub. And obviously it comes with all the goodies that Amazon gives us,
which has Multi AZ with Automatic Failover for disaster recovery if we don't lose the cache data. And support for Read Replicas.
So it's really good for you to know that there is this capability, not to lose the data if you have reboots,
it's called persistence. And also with a Multi AZ you can survive service outages. Now Memcached is a bit less popular but still available.
It's an in-memory object store and the cache, this time, doesn't survive the reboots. And the use case are to do
a quick retrieval of objects from memory and to cache often accessed objects. To be honest, and I did a bunch of research,
Redis is now largely growing in popularity and it is the dominant cache technology. It has a better feature set than Memcached
and overall it's gonna be the number one choice. But, Amazon still call ElastiCache. So personally speaking, if I need to do caching,
I would use Redis and not Memcached. But that's a personal preference. The AWS exam would never ask you
if Redis or Memcached is better or a better fit. Just know that ElastiCache, in general, is a cache
and just know about the use cases, okay? This was just me giving you a small insight into the technology you should use.
So that's it for ElastiCache. I hope that helps and in the next lecture, we'll just quickly go through the setup of ElastiCache,
just to see how it works.
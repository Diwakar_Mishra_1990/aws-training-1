Okay, so we are getting into the lecture, of how you can connect to your EC2 instance, and for this, we'll use the SSH utility.
So, based on the different operating system you have,

Mac, Linux, Windows less than 10, or Windows 10, you may be able to use the SSH utility.
So, the SSH utility works for Mac and Linux, but it does not work for Windows less than 10, and it actually works for Windows 10.

So, I have created two lectures, one for Linux and Mac for SSH, and the other one for Windows 10.

Then, if you have Windows less than 10, you need to use Putty. Putty is a drop-in replacement for SSH,
and so, if you are on Windows less than 10, you should use Putty, and if you're on Windows 10 as well,
you could use Putty. And, nonetheless, if none of these things work for you, there is something called EC2 Instance Connect,
and EC2 Instance Connect allows you to connect to your EC2 Instance directly from your browser
on any operating system. So, you may be asking me, "okay, which lecture "should I watch next?"
If you're a Mac or Linux user, you need to watch the lecture on SSH on Mac Linux. If you're a Windows user, you need to watch the SSH
on Linux Mac Windows, because there is something in it called chmod 0400 command,
and that is something you need to know going into the exam.
So, you still need to watch that lecture, even though you cannot do it on your laptop.
Then, you go into the Putty lecture, to learn how to do SSH on Windows using Putty. And if you are Windows 10,
then you should also watch the lecture called SSH on Windows 10. Finally, for everyone, I want you to also look
at the EC2 Instance Connect Lecture, because I think it is really cool, can solve many of your problems for SSH,
and allow you to keep on moving with the course, no matter what, okay? Now, SSH is what students have the most problems with
in this course. So, if things don't work, don't panic. Number one is re-watch the lecture.
You have no idea how many people just miss something in the lecture, and by missing a small detail,
then they fail and things don't work, so re-watch the lecture. Maybe you'll figure out something
that you haven't done before. Start over. If things still don't work, I've created a troubleshooting guide,
that I have at the end of all the SSH lectures, so go to the troubleshooting guide, and have a look at it.
It contains helpful tips on how to solve your issues in minimal steps. And if things still don't work,
and you can't do SSH or Putty, please look at the EC2 Instance Connect lecture.
And this lecture works for everyone, okay? So, if you don't get SSH to work on your machine, then use the EC2 Instance Connect lecture,
and you'll be able to do SSH from your browser, and be able to keep on going with this course.
All right, that's it. Hope you're excited, and I will see you in the next lecture.
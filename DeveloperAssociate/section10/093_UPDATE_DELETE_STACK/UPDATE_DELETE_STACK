And so we may want to just provide a new CloudFormation template to update this one. So this is what we're going to do.
We're going to go and update with this CloudFormation template. Now, as you can look, this cloud formation template is now 48 lines,
it's a little bit more. But if we just read through it, just with a quick glance we see that there is a parameter
in which we'll have to specify the security group description. So let's have a look at what that does in a second.
In terms of resources, we have our EC2 instance, but it has more parameters including some security group parameters.
We are creating a elastic IP, so EC2 EIP stands for elastic IP. And we're creating a security group, and we're creating a second security group.
So now this is a little bit more complicated. We went from eight lines to 48 lines of infrastructure S code.
But let's see how things work. We'll be able to understand this by the way with the next lectures. So let's go back to our stack.
And if you go to action you are able to update the stack. When you update the stack, you can either update it in the Designer tool,
or you can straight upload a template to Amazon S3, we'll just choose that one. And we're gonna click on this 1-ec2-withsg-eip.yaml.
Click on next, and now we get to specify some details. So just like before we have the stack name, but now it's grayed-out.
Indeed we cannot change the name of a already created CloudFormation template. So we'll just have to keep that name.
But now we get a parameter section that we did not have before, and that's because we did set up a parameter right here,
and it is asking for our security group description. So I'll just say this is a great security group. And we'll just leave it as-is,
you can put whatever you want, but this is something that me, as a user, I just created. Let's click on next.
We could specify some tags, we won't. Permissions, rollback triggers, et cetera. We will not change anything that was created from before.
I will click on next. And so now when we get to review the details, we see that by scrolling down, we can preview your changes.
So we are updating our CloudFormation stack, and now we have changes because we're doing an update. And so what is going to happen?
Well, we are going to have an EIP that will be added. We're also going to have some security groups right here.
Two of them that are going to be added, so created. And MyInstance, which is an EC2 instance, is going to be modified.
And it turns out that there is this replacement true column. So replacement true means that the previous EC2 instance
is going to be deleted and a new one will be created. Sometimes for different changes you'll get replacement false.
And so that just means that your EC2 instance is not going to be replaced or whatever resource type you have here.
So we get an idea of all the things that need to happen. There will be an EC2 instance going away, a new one coming in, new security groups, new EIP.
And so the idea is that CloudFormation is going to take care of all the complexity of doing these operations for us.
The only thing we've specified is just declaratively in this resource section what we wanted. And CloudFormation automatically figures out the changes
and what needs to happen in which order. Let's go and try that out, we'll click on update. And now we go to update in progress.
So update in progress says that the stack is being updated by the user. And so now we just have to wait and see what happens.
So we started at update in progress, but now we can see that there is a create in progress happening for some security groups.
And now we get an update in progress for my EC2 instance, and it says requested update requires the creation of a new physical resource,
hence creating one. So it went ahead and created a new EC2 instance, then it created for me an elastic IP.
And so if we refresh this one more time, we can see that the previous EC2 instance is now being deleted.
So we get a deleted in progress right here, so this is the last thing that needs to happen. Remember it was replacement true
in the changes that were being forecasted, and so that means that my EC2 instance gets replaced, and so a new one got created
and the old one is being deleted. So let's just wait one little bit of time. So now we have the update being completed.
And so the delete was completed for my EC2 instance. Let's have a look at the resource tab. And now in the resource tab we see four things.
We have an EC2 instance, but we also have an elastic IP, and we have two security groups that were created.
So this is quite great. If we got to the parameter tab we can see that for the parameter named security group description,
we gave it the value this is a great security group. So now let's go to the EC2 console, click on instances,
and now yes we see that one instance was being terminated because it was being replaced and the other one is up and running.
It looks like this EC2 instance is having a public IP, which is right here. And also in terms of security groups,
it has two security groups attached to it. Let's have a look at the public IP first. So I'll click on the elastic IP,
and yes we can see that this IP is attached to my instance, so this is great. Now if we go to the security groups,
we can see that we have two security groups that have been created, and the group name actually is prefixed
by MyCloudFormationTemplate, MyFirstCloudFormationTemplate, which happens to be the stack name. So as you can see the resources are prefixed.
And if we look at the inbound rule, for this one it has the two ports we've defined. If we look at the description the group description is
this is a great security group. And this is exactly what we just did as an input in the parameter,
so we can see the parameter we had trickled down all the way to the group description. If we look at tags, we can see again that the key,
and for our basically representative CloudFormation, and the link to the CloudFormation templates we have created, which is really really nice.
So we have these two security groups, and they're linked to my EC2 instance. So here we saw how to update a CloudFormation template,
which is very very cool. And now lastly, say we want to get rid of all these things. So we have created an EIP, an instance,
and two security groups. And we want to just get rid of them. Well instead of going to the EC2 console and right clicking and doing termination
and then deleting the security groups and then deleting the EIP, which would be a nightmare to do, we're going to just go in CloudFormation,
click on action, and delete stack. And basically deleting the stack will delete all the stack resources. So I'll click on delete,
and we'll get back to a delete in progress status. So if we go there, we can see that the resources are being deleted.
And on top of being deleted, they're being deleted in the right order. So we don't even have to think about
which order should we delete the stuff in. CloudFormation does that for us. And so we can really understand you know,
when we had the elastic beanstalk, we had something like 16 resources. And CloudFormation figured out how to create them
one-by-one in the right order. And then if we were to delete our elastic beanstalk template, again CloudFormation would figure out
how to delete this naturally. So CloudFormation is a great helper in the AWS stack. And so we just need to wait but now we can see
that the delete was completed for my elastic IP. And so if I wait a little bit more, everything should go away,
and my stack will go away as well. So that's it for the introduction to CloudFormation in which we did create, update, delete,
and understand the perks of CloudFormation. And also we had a small sneak peek into parameters, which was great.
So I hope that was helpful, and I will see you in the next lecture.


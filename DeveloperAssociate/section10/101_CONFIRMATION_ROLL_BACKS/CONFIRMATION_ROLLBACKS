So let's quickly talk about CloudFormation Rollbacks. It's very important to know how they work,
in case they appear in the exam. So, Stack Creation Fails, so if you upload a stack and the stack creation fails,
by default, everything will rollback. That means it will get deleted. So, we can look at the log to understand what happens.
But when you create the stack, you also have the option to disable the rollback, in order to troubleshoot what happened,
and get a little more insight into what was created. If you update a stack, so it was already created
and successful and now you update it. If the update fails, the stack will automatically rollback to the previous known working state,
which is the green state you just wanted to update, and you get the ability in the log to see what happened, thanks to error messages.
So, this lecture is all about showing you how rollback works. So, let's go ahead and recreate a stack, and we'll re-upload a file that we've been using before,
which was Just EC2 file. We'll click on Next and we'll just say, Failure Trigger Stack. So, we're going to trigger a failure in this stack,
but for now we're just going to make sure it completes, so we'll go all the way to Create, and then we'll wait until it's created
so we can trigger a failure. So my stack has been created correctly, and now we are in the Create Complete.
So let's go ahead and update that stack, and I will choose a template, and this template I will choose will be
the Trigger Failure template. So, this template is just like the one we had before, we can specify a group description, so I'll just say foobar,
just because I don't want to set up anything, click on Next, click on Next, and it tells us that we can preview our changes.
So, it turns out that it all lookslike just like the things we had before, so let's go ahead and click on updates.
Now we have an Update in Progress, and while this happens, I want to show you what I updated.
If you go and open the Trigger Failure.YAML file, you'll see that the Image ID is ami-123456. And so this is an invalid AMI.
Okay, no AMI is having the identifier 123456. So what'll happen is that the CloudFormation will try to create our EC2 Instance,
and it will say, wait a minute, I don't know what AMI this is. So this is what we should see right here.
So, the CloudFormation is happening right now, and we get an Update Failed. So, this is what happens, it says, okay,
I tried to update my EC2 Instance but it gave me a fail, and if we look at the reason on the right hand side,
it says the image ID ami, blah blah blah, does not exist. And so we get the error we want, Invalid AMI Not Found.
And so then we get an Update Rollback in Progress, and so anything that would have been created by this stack will be rolled back.
So let's look at what was created. While there was a security group right here that was created, so the SSH Security Group and the Server Security Group,
they were actually successfully created, create was complete, but if I refresh, now we see that because there was
an Update Rollback in Progress my security groups were being deleted. So basically, the stack just rolls back
to its previous known state, and in some previous known states, there's only one EC2 Instance. And so that's the important thing here,
you see that if you do an update to a stack that has already been created but the update fails, CloudFormation will attempt to rollback.
So yeah, this basically shows a failure, and so if the rollback doesn't work, you'll also get an error and say, I can't roll back.
So it's quite safe to use CloudFormation, because in case your update doesn't work, everything will just be deleted,
and the stack goes back to a state that you know worked. If you try to create a stack straight from that failing template,
so let's go and give it a try. So, I'll select my failing template, click on Next, and call it Create Failure Stack,
and I'll just put whatever for the Security Group Description, click on Next. And in Advanced we can see that now there is an option
to set the rollback on failure. And so it controls whether a stack is rolled back if a stack creation fails.
And so right now, it says, yes, so in case the creation fails, it will roll back. If I say, no, in case the creation fails,
it will just stay in that state, and so we can look at it in Troubleshoot. For this case I'll say yes, to rollback on failure,
click on Next, click on Create, and now we get a Create in Progress for that stack. And so let's have a look at what happens,
the security groups are being created right now. The security groups have been created, and so now the EC2 Instance should try to be created,
but you see again we get the same error message. It says, the image does not exist, and so we have a Create Failure,
and so everything starts to roll back, so the rollback is in progress, and it turns out that after the rollback,
all the security groups will be deleted, so let's just wait for a little bit. And here we have the rollback complete, so as we can see,
the security groups that were created from before got deleted because the EC2 Instance Create Failed. And so we get this, and if we look at it
the status is that the rollback is complete, so I can still look at, basically, my log to understand what happens and debug, but in terms of resources,
nothing was created, everything was deleted, so I could not Troubleshoot into it. And so to do the troubleshooting,
you have to change that option for rollback. So that's it for rollbacks, I think it's really important
to be across failures in CloudFormation at least once. And so if you click on that stack and you delete it,
to clean up, this one will work, in this stack we cannot update because the create failed, the only thing you can do is look at the log
to understand what happened, and then the only thing you can do is delete the stack. So yes Delete,
and now we get both deletion of the stacks, and this is how we clean up our environment. So I hope you enjoyed it,
and I will see you in the next lecture.
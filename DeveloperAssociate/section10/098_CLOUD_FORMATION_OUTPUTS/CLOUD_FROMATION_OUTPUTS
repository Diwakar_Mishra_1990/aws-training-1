Now let's talk about mapping. Mappings are fixed variables within your CloudFormation Template.
They have to be hard coded. They are very handy if you need to hard code some values based on
different environments you're in. So dev vs. prod, or regions such as AWS regions, or AMI types, etc ...
And as I said all the values must be written out explicitly in your template. As an example, here is a mapping.
And this is how you write it. We have a Mappings section. Then you have the name of the mapping.
Then you have a key. Underneath you have key's code name and values. So it's quite a low-level type of architecture.
To make it more concrete, we may have a RegionMap to map regions to AMIs. So we're saying okay, you're within us-east-1, us-west-1, or eu-west-1.
Based on if you choose a 32-bit or a 64-bit type of architecture. Here is the AMI ID you should be using.
So overall, this is just a hard coding saying, okay, based on where the template is being run,
this is the AMI I want to use. So when would you use mappings and parameters? Well, mappings are great when you know in advance
all the values that can be taken. So for example AMI ID. You can deduce them from variables such as
region, AZ, AWS Account, environment, etc ... Whatever you can think of. To me, they allow safer control over the template.
But if you need the values to be really user specific, then the user should input a value and you don't know in advance what it can be,
then you should use the parameters. Now to access the mapping values, there's this function called Fn:FindInMap.
Basically it returns a value from a specific key. The short-hand syntax is this one. We use FindInMap with a little exclamation point.
We have to give the MapName, and the TopLevelKey, and the SecondLevelKey. So three parameters right here.
That's something you should know for the exam, just the syntax of it, okay. So if we look at this little CloudFormation template,
we can see we have a RegionMap that we have defined from before. If we want to create an EC2Instance
and reference, get the right AMI ID, then we use the FindInMap function for the ImageID. The first one is a MapName,
so we'll use RegionMap because here it's called RegionMap. The second is that we wanna reference to
the AWS Region we're in. So we're going to use the pseudo-parameter we just talked about. And the Ref function.
So we reference the AWS Region, the CloudFormation template it's running in. For example, we're running in us-east-1,
then we are in this block. Then we say 32 as the SecondLevelKey. So we look at the 32 key, and we get the value from it.
We'll get this amI-6411e20d This is the one that will be selected. This is all you should know about mapping.
Just remember the syntax for the FindInMap function, and the fact that mappings have to be written out explicitly in your template.
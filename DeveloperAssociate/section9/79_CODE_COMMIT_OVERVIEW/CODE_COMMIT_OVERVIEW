Now, let's get into our first topic called, CodeCommit. And, so, version control is the ability
to understand the various changes that happen to your code over time. And, so, having a code repository,
basically, enables you to do version control. And, the version control, the most popular system that we have out there is called Git.
And, you may have heard of it. I think it has taken the internet by storm, and now every new developer, you know,
uses Git for version control. And, so, the Git repository can live on your machine, my machine, anyone's machine,
but it usually also lives on a central online repository. So, basically, this is where the users will get
their pushes and their pulls for their code. And, this is where the version control will happen, centrally.
And, the benefits of using a centralized Git repository is that you can collaborate with other developers.
So, if you have a big team of 10, 20, or even more developers in your company, it's great to use
something, a version control mechanism, such as Git. And, you want to make sure that the code is backed up somewhere.
So, if you have a new developer coming in, you don't just send him the code from your machine.
You tell him, hey, this is the central Git repository, go get it from there. And, you make sure that, you know,
anyone can view the code, even new developers, old developers. It's fully auditable, 'cause we know
who pushed code where and it is really an added benefit of version control. There's, obviously, way more features out of this,
such as roll backs, et cetera, but you get the idea. Once your developer and you start using version control,
I promise, you won't get away from it. So, this is our CodeCommit. And, where does CodeCommit come in?
Well, basically, we're a developer, and we want to push the code to code repository, as often as possible.
But, it turns out that when you start running a business, the Git repository can become quite expensive.
And, the industry includes GitHub, and GitHub is awesome when you do a open source development and you have free public repositories,
as unlimited as you want. But, if you start doing stuff for your company, it becomes paid for private ones, okay?
You can use BitBuckets, and there's other tools in the industry. But, basically, AWS came in and said, listen,
we're going to offer AWS CodeCommit and it's going to be also a Git repository, but, hopefully, less expensive.
And, so, the whole idea is that AWS CodeCommit repositories are private. Okay, this is something to be having
inside of your own organization. There's no size limits on the repositories, so it will scale seamlessly, if you have
one kilobyte of code, or if you have one terabyte of, no, maybe not one terabyte, but one gigabyte of code,
you know, it will scale seamlessly. And, it's fully managed. It's highly available. It's something that's quite nice,
so, you know, what you have is a guarantee, to have something managed, because we don't need to think about how to provision machines,
et cetera, et cetera. And, then, the code is only in your AWS Cloud accounts. So, that means, the code really
never leaves your infrastructure. It's always there. And, so, it probably makes your security team
and your compliance team happier about it, right? And then, finally, just to add onto the security features,
then we have encryption and access control, et cetera, et cetera. And, for terms of, like, if I'm working
in AWS, you know, am I locked into AWS? The answer is, no. It is definitely integrated with other tools,
such as Jenkins, CodeBuild, or other, third party CI tools. So, overall, CodeCommit is, basically,
a Git repository that's fully featured and quite secure. So, talking about security, because
it is such an important section of the exam, to understand. CodeCommit security is a bit complicated.
But, sometimes first forward, so, I'll do my best to teach it to you. Interactions are done using Gits.
So, it's still Git that you're using. It's standard commands, GitClone, GitPush, GitCommit,
all the stuff you've been using, Git on your machine will work with CodeCommit, right?
When you authenticate in Git, you have SSH keys. And, so, as an AWS user, you can configure
your own SSH key in your IAM Console. We will not do this in the lab. Or, you can use HTTPS.
And, it's using the same IAM Console. And, so, you can either use the CLI Authentication helper,
which I think is a little bit complicated, but you should know about it. And, you can also generate your own HTTPS credentials.
This is what we'll do in the labs, okay? And, finally, if you want extra security on the authentication, you can use MFA,
so multi-factor authentication. And, that just makes sure that your users are really the one you trust.
So, that's all for authentication. For example, for GitHub, it also has SSH and HTTPS. Now, authorization.
So, when you're authenticated through Git, you also need to be authorized to do stuff on your Git repository.
And, so, for this, you can use IAM Policies. And IAM Policies, they help manage your IAM users or IAM roles, basically
to make sure that they have the right rights, to your repositories. For encryption, we have the repositories
are automatically encrypted, at rest, using KMS. So, anytime you see encryption at rest, think KMS.
And that's automatic, you don't even have to set it, it's just done for you. And, obviously, because we use HTTPS or SSH,
they're, all of the S in it, stands for secure. And, so, that means that your data, your code, is encrypted, in transit, when you push it.
Now, if you want to enable cross account access, that's a very common question on the exam, as well.
How do I give access to my CodeCommit repository to someone else, you know, in another account.
Well, do not ever share your SSH keys. That's a security risk. So, don't ever do this. That's bad.
Just like your credentials, as well. Do not ever share your AWS credentials. This is also really, really, really bad.
The right way of doing it is to use an IAM Role, in your AWS accounts, and then, the other person has to use AWS STS.
It's, basically, a cross-account access API, called AssumeRole. And, basically, they will be able to access
your CodeCommit repository. You don't need to know exactly how to do it. You just need to know that, to give
someone access to your repository, you use STS and an IAM Role. Okay, so common questions around CodeCommit and GitHUb.
So, the similarity's that their both Git repositories. They both support code review, so it's called pull requests.
And, they're both integrated with AWS CodeBuild, which is nice. And, finally, they support HTTPS and SSH
method of authentication, both, so theirs is quite similar as well. So, as a developer, you shouldn't be,
it shouldn't change anything if you work against CodeCommit or GitHub. Now, there's a few differences still,
on the security side, mostly. So, GitHUb is administered through something called GitHUb Users.
Where as CodeCommit, you will directly use your AWS IAM roles and users. And, that's quite nice, because
when you have different user, you know, centralizing to AWS, it's easier to reason about, and to manage,
then having users in two different platforms. Now, the hosted is different. When you use GitHUb, it's going to be hosted by GitHUb.
So, it's a third-party, and you give, basically your code, to a third party, so there is a bit of trust happening.
If you use GitHub Enterprise, that means you have to self-host GitHUb on your servers, and in this case,
it's not managed anymore. You have to manage your own servers, and, so, you lose that aspect.
And, CodeCommit, well, it's managed by AWS, and hosted by AWS. So, you have maybe more safety,
and more security out of it. In terms of the UI, though, GitHub is a clear winner.
I find the UI to be fully featured. The CodeCommit UI is a little bit minimal. Finally, a very popular question on the exam as well,
is around CodeCommit notifications. And, so, I will give you the answer right here. It is quite confusing.
So, CodeCommits can integrate with AWS SNS, which stands for Simple Notification Service,
or AWS Lambda, or AWS CloudWatch Event Rules. And, so, based on what you're trying to do,
you will be able to integrate with one of these service, or the other. So, the use case for SNS and AWS Lambda notifications
is that you can alert for deletion of branches. You can trigger for when someone pushes code,
for example, in the master branch. You can notify external Build System. And, you can trigger, you know, an AWS Lambda function,
and that function can literally perform codebase analysis, to maybe see if, somehow, by mistake, someone committed AWS credentials
in the code, or any credentials at all, and pick that up right away, okay? So, anytime something happens to the code, basically,
think about it, every time someone added stuff to the code, you can integrate with SNS and AWS Lambda.
But, for CloudWatch Event Rules, and it's a bit more confusing, it's more around pull requests.
So, if a pull request updates, so someone created a pull request, and a pull request is, basically, a code review.
So, someone created a code review, updated it, deleted it, or commented on a pull request.
And, also, whenever someone commits code and comments on top of it. So, not when the commit happens,
but when a comment happens on top of the commit, and, that could be confusing words, as I know,
then you can have CloudWatch Event Rules, okay? And, the CloudWatch Event Rules,
to add onto the confusion, they will trigger something, like a notification, into an SNS topic.
So, I know this can be confusing, but this is something you have to know.mJust remember, basically, the difference,
but we'll see this in the hands-on, in the next lecture, when we use SNS and AWS Lambda,
versus when we use CloudWatch Event Rules. So, I hope that was helpful, as an intro to CodeCommits.
Let's go ahead and go with the hands-on now.
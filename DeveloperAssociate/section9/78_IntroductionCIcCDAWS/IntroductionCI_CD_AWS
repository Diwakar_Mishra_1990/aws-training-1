So this is one of my favorite section. As a developer on AWS it is so important for you to know about CICD.
And I love CICD, it really allows me to be a better developer and I'll tell you what it is about in a second.
But, so, setting up is always painful and I'm so glad that there is manage tools on AWS to do it.
So, first of all what is CICD? Well, we know how to create resources in AWS manually we've done this in the fundamentals,
we've created everything. And we know how to interact now with AWS programmatically as well, we know how to use a CLI
and that's a little bit better right? And we've seen how we can use Elastic Beanstalk to basically bring all these things together,
so we have less manual work to do. But still, when we deploy To Beanstalk, even if we use a CLI,
there was still a lot of manual steps. And all these manual steps, you know from my personal experience,
and probably yours, when there's too many manual steps there is a high likely-hood of doing mistakes and errors.
And so this is what we'd like to get to. We'd like to just push our code, you know change the code straight from our computer.
Push it somewhere, we call it a repository, and automatically we want things to happen magically, such as the code is on AWS.
That's how much complexity we'd like to have at home. So we want things to happen automatically, the right way.
Making sure everything, your code, you know, is tested before it is deployed. We don't want to push bad code into production.
And we want the possibility to try out different stages, dev, test, pre-prod, prod, or how many stitches you may have. Okay?
And sometimes we may want to have manual approval where needed. For example. Okay, there's a long testing happening
and before we listen to prod we want our bosses approval. So to be a proper AWS developer we're going to need AWS CICD.
So, CICD is continuous integration, and continuous, sometimes deployment, sometimes delivery, but this section is all about
automating everything we've done so far to increase safety. And the AWS Certification has a whole part dedicated to CICD.
So it's extremely important for you to understand and practice because it is really really key to passing the certification.
We're going to learn about CodeCommit, to store our code. CodePipeline to automate the pipeline deployment from code,
to all the way to Elastic Beanstalk. We'll use AWS CodeBuild to build and test our code. And we'll use AWS CodeDeploy
to deploy the code to EC2 fleets not Beanstalk, and so this is like a slight distinction I'll make during the course later on
between CodePipeline and CodeDeploy. So, Continuous Integration, what is it? Continuous Integration is basically allowing developers
to push code to a code repository as often as possible. And that code repository can be GitHub, that you may be familiar with,
CodeCommit that we'll learn in this course, Bitbucket, etc... So, here is me. I'm a happy developer.
Look at me I'm smiling, and I push my code often to my code repository. Now, it turns out
that we're also going to have a test / build server, and it will check the code as soon as it is pushed.
So very often it will just look at it, and for this we can use CodeBuild, or the most popular one,
at least on the open source world, is called Jenkins, but there are other obviously tools out there.
And so the build server just gets the code from the code repository and every time we do a push
it is going to build and test it. Obviously we have to set that up, but that's the idea right?
And then for the whole loop to be complete, the developer, so us, will get feedback about the tests
and checks that have passed or failed. And so, the build server comes back to us maybe with an email,
maybe with a user interface, UI, you can look at, but the developer can get the results of the build
and so the developer can spend more time creating code and pushing often and then the build server
takes the work to just check what is happening. So the goal of this is to find bugs early
and fix them early as well. It's to deliver faster as the code is tested continuously. It's to deploy often in the end,
so we'll see this in the next slide. And it's to make developers happier, as they're always unblocked in their workflow.
They don't have to spend time waiting fifteen minutes for their code to be tested. There is a build server doing that just for them.
So Continuous Integration, overall, is going to increase your productivity by a lot. And then, finally we have Continuous Delivery.
So, we want to ensure that the software can be released reliably whenever needed. And we want to make sure that deployments happen often,
and they're quick. Right? We don't want to wait, three days. We don't want to ship one release every three months,
or we just want to move that from that concept, you know, from hard releases every three months, all manual,
to let's release five times a day, or even more. You know some companies release every hour. That usually means that you have to automate everything.
You can't have someone doing your release every hour easily. And so that whole automation around deployment
is what we're looking into. And so for this we can use CodeDeploy, or Jenkins CD, or Spinnaker, or other tools.
I mean there's a bunch of tools available out there. In this course, obviously, we are going to learn about AWS CodeDeploy.
So what does it look like? Well it looks like we are doing the same thing, so we push the code often
and then the build server will get the code and build and test, that is from Continuous Integration.
And then we'll have a deployment or delivery server, and it will basically deploy every build that passes,
okay, and that's a simplification, but you get the idea. And so we had our application it was running version one,
or you know, the previous version, I'll just call it version one. And so the deployment server will go
and run a bunch of scripts that we have to program, obviously, but it will make sure that the applications
go from version one to version two. And that will happen from version two to version three etc.
For every time that we push the code to the repository. And so this whole pipeline right here that's happening
is really what Continuous Delivery is about. It is about deploying as often as possible, predictably and reliably,
because the day you actually want to go to production you want it to work very quickly and deterministically.
So, technology stack for CICD. It's something that AWS shows and so I've got inspired by them on this graph,
but basically there is five steps for this. There's Code, Build, Test, Deploy, and Provision. And so for Code we can use AWS CodeCommit
and we'll learn it in this course, and GitHub or a third party code repository. For this course we will not be using GitHub
but if that's your tool of choice then go ahead, use it. Then to Build and Test we have the option,
on AWS, to use AWS CodeBuild. Very easy, and so this can do both builds and test. And usually these steps are quite similar
and bundled together. Or we can use something like Jenkins CI or a third party CI server. Obviously, there is always the choice to move away from AWS.
Then when you are on Deploy, we have already seen AWS Elastic Beanstalk and it's been working great,
so Elastic Beanstalk as we've seen has provisions, the EC2 machines, the OtisCallen Grooves,
that load balancers for us, and on top of it every time we upload a new zip directly to Elastic Beanstalk it provisions,
you know it deploys the applications straight onto our servers and maybe sometimes provisions new servers
in case we choose immutable deployment. So this is good Elastic Beanstalk handles both. But sometimes you are managing your own
fleet of EC2 instances, maybe using something called cloud formation, that we'll see later in this course,
and so the idea is that to deploy to a user managed EC2 instance fleet, you want to use something called AWS CodeDeploy,
which is more low level and a bit more difficult to use but still good to know about. And so that's the whole, you know,
Code, Build, Test, Deploy, Provision, and because we need to orchestrate all these things, you know, we have to use something for orchestration
called AWS CodePipeline. Surely there will be other orchestration tools out there, but the one AWS wants you to use on their Cloud is called
the AWS CodePipeline. So in this section as you can see everything that is in orange we have to learn,
so there is a lot to learn ahead, but you get the complete picture of how all these things fit together
and that's really important when you get questions in the exam to know which tool is appropriate for which tasks.
So I hope that was helpful as an introduction to CICD and I will see you in the next lecture.
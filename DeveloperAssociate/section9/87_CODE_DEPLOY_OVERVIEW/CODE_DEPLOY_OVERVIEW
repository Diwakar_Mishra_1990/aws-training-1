So we are getting to the final type of deployment technology for the exam on the certification
but this technology called CodeDeploy is to me at least the most obscure one
and the less concrete and it's really hard to make sense of because it's really powerful
and it's a good abstraction model but it's tough to get a grasp of honestly. The exam doesn't ask that many questions about it
but it's still good for you to know a high-level overview. So I propose a high level overview
that's gonna be not too deep into it and then we're gonna do two small hands-on lab on CodeDeploy.
So CodeDeploy is in the context of we want to deploy our application automatically to many EC2 instances
and we're talking about hundreds of EC2 instances. So we have our V1 EC2 instances and we upgrade them to V2.
Very simple concept. Except these instances are not managed by Elastic Beanstalk So everything we see with Elastic Beanstalk
before there was one way to manage EC2 instances. These EC2 instances for CodeDeploy they're basically managed by us.
We've created them from before and so there's several ways to do this using open-source technology with various mainstream
or non mainstream usage but Ansible, Terraform, Chef, Puppet etc. All these technologies allow us to basically deploy
application onto EC2 instances but for CodeDeploy, we may wanna use it because it's a managed service and so it runs directly
from the AWS console and environment. Now how do you make state CodeDeploy work and how does it really act right?
So each EC2 machine and I think that's one of the most important thing you need to know about CodeDeploy at least for the exam.
They can be an EC2 machine or it can be an on premise machine by the way and it must be running something
called the CodeDeploy agent. So it's like a little software running on the EC2 machines and so the agent is basically going to do some things
going to continuously poll for the AWS CodeDeploy service and ask it A, do I have to deploy new stuff?
Do I have work to do? CodeDeploy will send the appspec.yml file or will point to it at least the application we pull to
from GitHub and S3 and EC2 will run the deployment instructions and CodeDeploy agent at the end of it will report whether or not
there was a success or a failure of the deployment on the instance. So it's got a strap but here is me
a happy developer, I have my source code and I have my appspec.yml file. So that file is super important
you'll get asked about it and I have a whole lecture on it anyway whole slide but basically that file
has to be at the root of your source code and it will define how the application gets deployed
and so as a developer, I'm going to go ahead and push my source code to GitHub or Amazon S3
and then I'm going to trigger a deployment on AWS CodeDeploy. Now my EC2 instances because they continuously pull
for CodeDeploy, they will realize that I've triggered a deployment and say okay time to deploy.
So they will download the code and the appspec.yml file onto the EC2 instances and the agent will take care
of running whatever is in that appsec.yml file to basically deploy your application correctly.
So other things you should know about CodeDeploy is that the EC2 instance are grouped by something called deployment group.
You can have dev, test, prod or just different segments. You have a lot of flexibility as to regarding
as to what you want to call the deployment groups or deployments. You can integrate CodeDeploy with CodePipeline
and use the artifacts straight out of CodePipeline to deploy, you can reuse existing set of tools
like anything that you have on your EC2 instances basically can be reused by CodeDeploy. It works with any kind of application
and it has auto scaling integration. So CodeDeploy is basically so that's something that's a little bit more powerful than Elastic Beanstalk.
Elastic Beanstalk, I really forced you to use a set of different applications and platforms. CodeDeploy can be anything you want.
And so behind that power there's a bit of more complexity. You can do Blue Green deployments but it only works with EC2 instances
so not on-premise instances and there is a unknown like little less advertised support for AWS Lambda deployment.
It's not something they will ask you at the exam but we'll see how this works when we get to a AWS Lambda. Finally, something you should know,
CodeDeploy only deploys your application. It doesn't provision the resources. So it assumes that your EC2 instances are already existing.
In terms of what goes into CodeDeploy, there is an application, there's a compute platform which is either EC2 or Lambda.
There is a deployment configuration where you define the rules for success and failures and so if it's EC2 On-Premise you can specify
the number of healthy instances or the deployments. Say for example you want 95% of the deployment to succeed
and for AWS Lambda you just say how you want traffic to be routed to Lambda functions. For deployment groups, you can group tag the instances
and you can define an in-place or Blue Green deployments. So for IAM profile you basically need to say hey,
my EC2 instances must have the IAM role necessary to pull the files from S3 and GitHub. Application revision is basically
any time you update your application and you provide code and appspec.yml file you're going to create a your revision,
service role is what CodeDeploy needs to perform the deployments and target revision is basically where you want like the target version
of your application when you upgrade it. These are just words to know about. You just need to see them once,
you don't need to learn the definitions but general just to give you an idea about what they are.
Now the appspec.yml file to me is going to be the thing you should know the most and so there's two sections.
There's the file section basically how to source the files, the source files and then how to copy them from S3 GitHub
to the file system and then there's going to be Hooks and Hooks is going to be set of instructions or commands
and you can use to deploy the new version and you can put timeouts on Hooks if you wanted to just say
Hooks should not run for more than two minutes, otherwise fail. And so the order of Hooks is what you should know definitely.
So the first Hook is to stop an application. So Hooks finger out is like when your new version gets deployed.
So we have our version running currently and if you think about it this way, we have a version running currently
and the first thing we want to do if we want to deploy a new version is to stop the current application.
So there's a StopApplication hook. Then once the application is stopped, there is a DownloadBundle hook.
So basically saying how do I download my new application? Now there is a hook called BeforeInstall and this BeforeInstall hook allows you to do any preparation
before installing your application version then your application version installs and there is an AfterInstall hook that happens
which do you wanna do any cleanup after your install or launch a server or something like this? And then you have ApplicationStart
so how do you start your application? And then finally ValidateService. So once your application is started,
how do we make sure that your application is indeed working. That's really important because that's kind of like
the health check to make sure that this your application got correctly deployed. And so not all these steps must be specified.
You can specify as many as you want but you need to remember the order of them and to me, it's pretty natural.
You just run through the order on your mind and it kind of makes sense. So the exam will ask you about
the order of these hooks for example. Now this is an actual deployment that I did and we can see and we'll see this in the URL
I'm going to do the hands-on but we can see that there is all the events that happened in your CodeDeploy.
So ApplicationStop, DownloadBundle, BeforeInstall, Install, AfterInstall, ApplicationStart, ValidateService and then we get some CodeDeploy specific.
BeforeAllowTraffic, AllowTraffic and AfterAllowTraffic. So there's a lot of different stages to your deployment
but you basically get control over a lot of them. Now for deployment configuration, you can say I want to deploy to one instance at a time
and if one instance fails, well the deployment should stop. You can say deploy to half at a time so 50% and then another 50% or all at once.
It's quick but then you just get no healthy host. You get some downtime and it's good for development.
And you can also set maybe a custom config saying the minimum number of healthy host should be 75%.
So you get a lot of different configurations. In case of failures, the instances will stay in failed states and if you do a new deployment,
the deployment will first be deployed to failed state instances which guarantees that you won't bring down your whole application
because of a failure and if you want it to roll back to a previous version, you should just redeploy the old deployments or enable
automated rollback for failures. Just different configurations again. Deployment targets can be a set of EC2 instances
and you assign tags to them or you can directly deploy it to an auto scaling group or you can do a mix of auto scaling group and tags
and you can create deployment segments so you get a lot of power over how you want to customize your deployment
and then finally, if you want it to be super advanced in your script, you can use an environment variable
called Deployment Group Name and basically to say okay if I'm in dev do this, if I'm in prod do these things.
So you get a lot of control. Now you don't need to know all these things to be honest. You should just know that a deployment target
is EC2 instances with tags or an ASG and that for example in case of failure when you redeploy, it redeploys to the failed instances first.
In terms of In Pace Deployment, I will show you what half of the time looks like. We have four instances.
We're gonna take the first two down and deploy the application there and then we're gonna take the second half down
and then deploy the application there and we get from V1 to V2. So that's very simple.We've seen this in Elastic Beanstalk and as you can see,
the patterns are very very similar is because I guess Elastic Beanstalk uses CodeDeploy in the under the hood but you don't see it.
Now for Blue Green deployment, basically you have a load balancer. It's attached to one auto scaling group of instances
and then you're going to create a new auto screen group of instances and the load balancer is going to redirect to both these things
and then if everything succeed and they have instances, pass the health checks, then the first auto scaling group is deleted
and the load balancer just talks to your V2. So it's just an idea. So that's it. That's all you should know about CodeDeploy
and now we're just gonna do a few hands-on just to get a feel for it but at the exam just remember appspec.yml file where it is,
the order and then how the deployment groups work. So I hope that was helpful and I will see you in the next lecture.


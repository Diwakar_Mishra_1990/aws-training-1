So at the exam, there might be one questions asking you the difference between CodeStar and CodeCommit. So I thought I would just make a quick lecture
on CodeStar to help you understand what it is. So CodeStar is going to be an integrated solution that regroups all the bunch of CICD services we've seen.
GitHub, CodeCommit for storing code, CodeBuild for building the code, CodeDeploy and CLoudFormation for deploying the code,
CodePipeline for handling the pipeline orchestration, and CloudWatch. So, it brings it all together. It's just a wrapper around everything
and gives you a nice one-stop dashboard for this. So it helps to create very quickly, CICD-ready projects
and that you can deploy them on EC2, on AWS Lambda, or on Elastic Beanstalk. It supports many languages, C#, Go, HTML 5,
Java, Node.js, PHP, Python, and Ruby. And there is an issue tracking integration that you can do with JIRA if you use JIRA on your enterprise
or GitHub issues if you use GitHub as a source code repository. You also have the ability to integrate with Cloud9
and they will give you a web IDE, so a web environment to edit your code. That's not available in all regions.
Otherwise, you can check out your code using the command line like a GitHub repository directly hosted in CodeCommit for example.
You get one dashboard to view all of your components in CodeStar. So it's an integrated view, and it gives you
insides into your CodeCommit, your CodeBuild, CodeDeploy, CodePipeline, and so on, and your CloudWatch matrix as well
so you can have a one-stop for your application managements. It's a free service; you're only going to pay for the underlying usage
of the other services, so it's great. And there is limited customizations so you cannot edit every single setting
of every single underlying service. It's meant to be simple and get you started very quickly so that's exactly what it does.
All right, let's go into the hands on. So let's go to CodeStar and here this is meant for giving you an integration point
to quickly develop Build and Deploy applications on AWS. So let's start a project, and for this you need to create a service role.
So yes we are going to create a service roll for CodeStar and here we go we're done. Now you can choose the programming language you want,
the application category you want, and the services you want. We'll choose Beanstalk because you have been using Beanstalk forever.
And now we're going to use Node.js to edit. And I'll call it my CodeStar project. So as we can see as we create the project
it chooses what repository do we want to use for storing the code. So CodeStar does use AWS CodeCommit or GitHub to store the code.
So we'll use CodeCommit, and so the difference between CodeStar and CodeCommit is that CodeStar uses CodeCommit but not only.
All right so let's click on next. So here we're able to see for that one template what is happening. So we have the source in CodeCommit,
we have build step, test step, then deployment step using Elastic Beanstalk, and some monitoring integrated we have CloudWatch.
And all in all it's all orchestrated with an AWS CodePipeline pipeline. Okay excellent, we could edit the Amazon EC2 Configuration
but for now we see they only have 3 parameters for this so this is quite limited configuration. We'll keep the t2.micro because it's on the free tier.
Okay, create the project and you choose a key pair, so I'll choose online courses, create my project, and here we go.
Now it looks like it's our first time here so I'll just give my display name as Stephane and my email at stephane@example.com, and here we go.
So here we have 3 different tools we can use to edit our code, and actually if we were in U.S. East North Virginia,
Northern Virginia, we would see Cloud9 as well, but here it's not available to us. That's okay, we'll just use the common line tool,
and just the same way we'll do our Git checkout from CodeCommit. So I've clicked on skip, and now
my CodeStar project has been successfully created, And this is our dashboard;I'm going to close this.
And let's go back to our dashboard from CodeStar, here we go. Now we can see we have a lot of different visuals in there, in this dashboard.
So we can see that there is a continuous deployment pipeline here done by CodePipeline. So here we go, we can see that the source
went into CodeBuild, then it's going to be deployed using CloudFormation into a Beanstock environment.
We could be looking at the Cloudwatch at activity right here, and so on. So the cool thing here is that CodeStar is linked to all the underlying services.
So I clicked on code and I end up in CodeCommit. I could click on build and I would end up in CodeBuild.
I can click on pipeline and end up in CodePipline. So here we see that AWS CodeStar is integrated with all these things.
So what we could do if you wanted to is just check out the source code into a directory, change the code, push it back, and automatically
all this yes, CD is already configured for us, so we don't have to do all the manual steps that we did in the previous lessons.
So really that's it just for this quick demo. I won't do a CodeCommit demo in front of you because we've already done that.
But if you wanted to play, you could change the source code into this application, push it back into CodeCommit, and see if the change
gets propagated all the way through here. Lastly I just want to show you a few things. The extensions so here you can integrate
with GitHub issues or Atlassian JIRA. And finally for the project, we get all the information
of all the ARN of the resources for the project. So CloudFormation, CodeBuild, CodeCommit, CodePipeline,
or the IM roles that are used for different services as well as amazon history bucket for storing some code and artifacts
in the CodeBuild parts of this project. So that's it, hope that was helpful and don't forget to delete this project at the end of it
once you've had a play with it. And for this you just type the project ID and you are done. Here we go so that's it, I hope you liked it,
and I will see you in the next lecture.
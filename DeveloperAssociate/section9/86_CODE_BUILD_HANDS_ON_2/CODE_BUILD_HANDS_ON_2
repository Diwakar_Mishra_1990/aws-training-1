Okay so back at it we need to fix that YAML file error because our build spec.YAML file does not exist.
So let's go to CodeCommit and we're going to create a file. Obviously we could do this using the Git
command line but this is going to be easy to do in-browser and just for the sake of this demo
this is just fine. Okay, so now we need to add this file content. So the file name, first of all, is build-spec.YAML, okay?
Now, for the file content. I'm not going to type it out. I'm going to copy it straight from the code
you've downloaded. So right here and I'm going to describe to you what this means. So this is a YAML file and a YAML file is made
of text and colons. It's a key value pair type of file. And you need to get used to this format but it's quite human-readable, and that's why
people like it. And so two separate sections. You can add spaces or tabs and this will indent your thing. Okay, so now let's look at this YAML file.
The version of our build spec dot YAML is 0.2 and this comes straight from the reference documentation for this build spec dot YAML file.
Now we have phases and I just added a bunch of commands into each phase to show you how they work. So the first phase is install and here we specify
the runtime version for our CodeBuild environment. So here we have node JS and we use version 10. We can also insert commands that will be run
while CodeBuild does its test. And so here during the install phase just echo, which means print to the screen, installing something.
And so it's indeed installing node JS 10. Okay, pre-build. Before the build happens we can also do a bunch of commands.
So we can do commands everywhere and I will just say again, echo. We are in the pre-build phase. Now, while it's building this is where
the important commands are. This is where you're going to test out your code. So here you can set up a bunch of commands.
So I said, "We are in the building block. "We will run some tests." And here this one command right here,
don't need to be a Linux expert, I'll just tell you what it does. Grep minus FQ congratulations index of HTML
will look at this index dot HTML file and see if the text congratulations is in it. If it's in it, it's going to succeed.
So this test is going to pass. But if congratulations is not found in it anymore this is going to crash and fail.
So this is our test. Then we have post-build and we could just echo. We are in the post-build phase. Okay, so this is the kind of files
that you'll build over time and you'll get used to it. Okay, so build spec dot YAML. The author name is Stephane and the email
address is stefane@example.com, and the commit message. I just added build spec dot YAML at the root of our directory.
So this is super-important to say that it's the root of our directory because this is where the file is expected to be.
We'll commit the changes and now build spec dot YAML has been committed. Excellent. So if you go back to our my node JS app
we see that build spec dot YAML is here. So now if we go to CodeBuild, go to our build project, go to this one and have a look.
Okay, the build history has failed but now what we can do is start a new build. And okay, the same configuration as before.
Start the build but now hopefully this build should pull that file that we've created, build spec dot YAML, and run our build.
So let's wait and see what happens. And this time the build has succeeded. So let's look at the phase details.
So we can see all the phases that have happened in our build. And it goes all the way from submitted to queued,
provisioning, download source, install, pre-build, build, post-build, offload effects, finalizing, and completed.
Oof, I said them all. And we can see how long each phase did take, okay, with a start time and the end time.
So this is a really good visibility into what could be taking a long time if we ever have a timeout. Okay, now let's go into the build log
and this is showing the 10,000 last line of the build log. But we can view the entire log if we clicked on this.
And this would take us straight into the cloud watch logs UI to see all the logs. So let's look at them from here.
This is pretty interesting. So here it says entering phase install and is going to install node JS version 10.
So it says that, okay. This is the version we specified from before. Then we echo installing something which is just our little debug to see what happens.
Then the install phase is succeeded. Then we go into the pre-build phase where we are in the pre-build phase
so this is returning right here, again as a log line. And then we succeeded. We go into the build phase.
And we're in the build block. We're going to run some test and we run the command grep minus FQ congratulations index HTML.
And this passes, this succeeds, so the build phase is succeeded. And finally we're into post-build and we echo.
We are in the post-build phase. So we get the access through the whole log either from CloudWatch logs or again directly
from the CodeBuild UI and we can see the exact same thing. Okay, so this is a good news. Now how do we integrate this build project
into our code deploy? Pretty easy. So let's go to our deploy, not a go to deployer, go to pipeline obviously.
I'm getting ahead of myself. So let's go to my first pipeline. And here between source and deploy I want to test.
So I'm going to edit my pipeline and I'm going to add a stage. And I'm going to call it build and test.
But here we're just going to test, okay but we could build as well if you wanted to. Okay, within this we're going to add
an action group, call it test for congratulations. And the action provider is going to be CodeBuild.
In this region, input artifacts is the one from the source. The project name is my build project
and for the output artifact we can just choose a name for the output of the section. We'll call it output of tests.
Okay, excellent. Click on done and here we go. We have added our CodeBuild into this entire pipeline.
So I'll click on done and we are done here. It looks like it. I'll save this and save that. Okay, so now we have a new pipeline.
And so we should be testing it, right? So how do we do this? Well, we go to CodeCommit, for example,
and we go to our index dot HTML file. We're going to scroll down and find this congratulations text.
And we're going to edit it out. So I'll click on edit and I'll scroll down to congratulations, and I'll change it
to something like horrible. And I'll say, "Okay, this is Stefan, stefan@example.com. "Change text from congratulations to horrible."
And the reason why I changed this congratulations text away is to horrible, I could have said whatever,
is because we want to have the text fail. So I'll commit my changes and changes have now
been committed to master. So back into my code pipeline. Very soon I should start seeing my CodeCommit to succeed.
So let's wait a second to see if things get started. Okay, so here at my source data was just pulled.
And then we went straight into the build and test, which is in progress. So I can click on details to see what's happening.
So it takes me straight into my CodeBuild UI. And this has failed. Okay, so my build has failed. And if we scroll down and see why it says, "Okay,
"This grep minus FQ congratulations index of HTML "has failed because it did not successfully "find congratulations in my index dot HTML file,"
which is exactly the behavior we wanted. So this has failed and so therefore my bad code has not been put into Beanstalk.
Beanstalk still shows the congratulations text. So how do I make this pass, the test suite?
I'll go back to CodeCommit and I'm going to fix obviously my mistakes. So I'll say, "congratulations CodeBuild."
We're just going to add a little funkiness to it. And I'll say, "Stefane," same address, and then I'll say, "Fixed the congratulations texts."
Commit my changes and here we go. It's been committed to master. So now very soon my source should get started
to pull the new code from CodeCommit. And hopefully now this time build and test will pass.
And then this change will be deployed into Beanstalk. So let's wait a little bit again to see this.
Okay, so this time the source has passed. The build and test has succeeded so my test suite has worked.
And has been deployed, and so if we go into Beanstalk and refresh we now see, "Congratulations CodeBuild."
So CodeBuild did its job correctly. So I hope you can start seeing the power of CICD right here, how easy it is to change things,
test for them, and then push them eventually to whatever environment you need to push them into. So back to CodeBuild, we have our build projects,
we can look at our build history and see how long each build was, when it was completed, if it succeeded, why it was invoked, and so on.
And so overall with, I think CodeBuild is a really awesome tool to have, lots of different options.
But you've seen the main ones going into the exam. So I hope you like this lecture and I will see you in the next lecture.
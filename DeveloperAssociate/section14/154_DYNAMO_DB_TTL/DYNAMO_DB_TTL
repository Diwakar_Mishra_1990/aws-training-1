So a few additional lectures on DynamoDB because there are some more advanced questions now that are being asked of you at the exam.
So the first feature I want to talk to you about is the TTL or Time to Live. The TTL means that you define a column
and based on that column, the items that have an expiry date based on that column will expire and get deleted from the table.
Hence the name, Time to Live. So the Time to Live feature in DynamoDB is provided to you at no extra cost and the deletions will not use
any of the WCU or RCU. It's a background task and the DynamoDB service itself does it periodically
and why would you use TTL, well maybe to reduce the storage and manage the table size over time
because at some point, maybe you don't need a row to be in DynamoDB because it would have expired.
It could also help you to adhere to regulatory norms. For example, if you need to keep (mumbles) only for seven days in some table.
And TTL must be enabled per row so you define your TTL column as we'll see in the hands-on
and we'll add a date there and we'll see in which format in a second. And then, on the pro basis,
DynamoDB will delete these rows based on the expiration. So typically, it can happen pretty quickly but the guarantee
is that DynamoDB will delete or will try to its best to delete a row within 48 hours of expiration,
so it won't happen the second after the item expires but within 48 hours it should happen.
The deleted items due to TTL will also be deleted from any indexes, so your GSI or LSI.
And if you wanted to somehow recover these deleted items, we just saw what streams is, so DynamoDB streams
upon a delete due to TTL. We'll have an event and maybe you can recover these expired items if you wanted to.
So that's it for all the theory. Let's just go into the practice to see how TTL works. So let's go to creating a new table
and I'll call it DemoTTL and I'll have user ID as my partition key.
I will not use the default settings. I will choose provisioned. I will not have auto scaling and I will just have
one read capacity unit and one write capacity unit. I'll choose the default for the encryption at rest
and click on Create. Okay, so here is my table. It is being created. And now lets go to items and I'm going to create an item.
So user ID, I'll say user_123. And maybe you'll have a name, it's Stephane. And here, I'm going to add an expire_on attribute.
Now you can name this column whatever you want but it has to be a number and you'll set expire_on. And I will choose expire_on name
because it's pretty obvious about what it does. So, now you do time to epoch on Google, and you have this website called epochconverter.com
which is a great website. It allows you to take a time stamp, for example this one, and it gives you a number which represents an epoch time
which is a number of milliseconds since the year '70. So, what we'll do is that we'll set okay,
this one will expire at time 58, human to date timestamp and here's my epoch timestamp. And so what I'm going to do is copy this here.
So this item should expire in five minutes. I'll create another item, maybe for user_234. And for its name, I will say Laura and the name is Laura.
And then finally, again I will add an expire_on number so expire_on, and this time I'm going to select
in one hour from now, so here we go. I paste this one here. Click on Save.
And now I have two rows with two expire_on attributes. So I go to Overview, and I click on Time to Live attribute
and I click on Manage TTL. Here, I will say that my column to look out for the TTL is expire_on
and I could enable DynamoDB streams if I wanted to receive that as an update whenever it gets deleted.
But here we can run a preview and say, show me the items that will expire by right now.
If I run the preview, it says no items are matching. But if I go to a few minutes from now and do a run review
it says I found one item this one that should expire by these dates. And if I go even further and run again the preview,
now Laura is also going to be expired. So that TTL attribute just works fine and you can simulate here
and when you're ready, click on Continue. And here we go, now it's being enabled. And so what will happen is that
as we add items to this table, they will get deleted by DynamoDB in the background due to the TTL value on this column expire_on.
So that's it, all you need to know. I hope that helps and I will see you in the next lecture.